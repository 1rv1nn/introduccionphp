# Introducción a PHP

1. **Triángulo y Rombo**
Programa encargado de imprimir un patron de * formando un Triangulo y un rombo en base a controladores de flujo.

1. **Formulario**
Programa encargado de registar a un usurio a través de un login.

1. **Expresiones Regulares**
Expresiones regulares para reconocer correos electrónicos, CURP válidos, números decimales, palabras con una longitud superior a cincuenta caracteres, y una función para escapar caracteres especiales en una cadena de texto.        

# Intructores
- @karlafm.dev
- @danielbg.dev
- @comando132

# Autor
Cruz González Irvin Javier

# Descripción
En este repositorio se encuentran programas básicos implementados en el lenguaje PHP para el programa de Ingenería de Software (DGTIC)

