<?php

/** 
*  Script encargado de realizar expresiones regulares. 
*  @author Cruz González Irvin Javier 
*/

//Realizar una expresión regular que detecte emails correctos.
$CheckEmail = "irvin@ejemplo.com";
$regex1 = "/^[A-z0-9.-_]+\@[A-z0-9]+\.([A-z]{2,3})/";
$result = (preg_match($regex1,$CheckEmail)) ? "<b>$CheckEmail</b>  es correcto " : "<br>$CheckEmail Es incorrecto </br>" ;
echo "</b> 1. $result </br>";


//Realizar una expresion regular que detecte Curps Correctos
//Que permita usar únicamente estos caracteres: ABCD123456EFGHIJ789
$CheckCURP = "CURP123456ABCDEF02";
$regex2 = "/(^[A-Z]{4})([0-9]{6})([A-Z]{6})([0-9]{2})/";
$result2 = (preg_match($regex2,$CheckCURP)) ? "<b>$CheckCURP</b>  es correcto " : "<br>$CheckCURP Es incorrecto </br>" ;
echo "</b> 2. $result2 </br>";


//Realizar una expresion regular que detecte palabras de longitud mayor a 50 formadas solo por letras
$CheckString= "anocheviunacabravestidadeuniformeconalas y cabeza de shrek";
$regex3="/^([A-z]{51})/";
$result3 = (preg_match($regex3,$CheckString)) ? "<b>$CheckString</b> es una cadena con una longitud mayor a 50 caracteres " : "<br>$CheckString NO es una cadena con una longitud mayor a 50 caracteres  </br>" ;
echo "</b> 3. $result3 </br>";



//Crea una funcion para escapar los simbolos especiales.
$CheckString2 = "/caden@ con + símbolos *especiales";

function escape($string){
	$string=preg_quote($string);
}
escape($CheckString2);
echo("<br> 4.$CheckString2 </br>");

//Crear una expresion regular para detectar números decimales.

$CheckDecimal = "0.2349191";
$regex4 = "/[0-9]+\.+[0-9]/";
$result4 = (preg_match($regex4,$CheckDecimal)) ? "<b>$CheckDecimal</b>  es decimal " : "<br>$CheckDecimal NO es decimal </br>" ;
echo "</b> 5. $result4 </br>";

?>