<?php
//Seguridad de sessiones paginación
session_start();
$varsession=$_SESSION['nCuenta'];
if($varsession==null || $varsession='' ){
    header('location:login.php');
    die();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>info</title>
    <link rel="stylesheet" href="CSS/style.css">
</head>
<body>
    <header>
       <div class="container__menu">
            <div class="menu">
                <nav>
                    <ul>
                        <lib><a href="info.php">Home</a></lib>
                        <lib><a href="formulario.php">Registrar alumnos</a></lib>
                        <lib><a href="validar.php">Cerrar Sesión</a></lib>
                    </ul>
                </nav>
            </div>
       </div> 
    </header>
</body>
</html>